"""Simple Picture Taking Script
Author: Evan Juras, EJ Technology Consultants
Date: 8/8/21
Description:
    This program takes pictures (in .jpg format) from a connected webcam and saves them in the specified directory. The
    default directory is 'Pics' and the default resolution is 1280x720.

Example usage to save images in a directory named Sparrow at 1920x1080 resolution:
python3 PictureTaker.py --imgdir=Sparrow --resolution=1920x1080

Updated/modified by daxm.
  - Used black and isort.
  - Change string formatting to f-strings.
  - Used string template for image filename.
  - Added argparse logic to provide access to either webcam or stream (but not both).
"""

import argparse
import os
import sys

import cv2

parser = argparse.ArgumentParser()
parser.add_argument(
    "--imgdir",
    help="Folder to save images in (will create if it doesn't exist).",
    default="Pics",
    type=str,
)
parser.add_argument(
    "--resolution",
    help="Desired camera resolution in WxH.",
    default="1280x720",
    type=str,
)

# Create a mutually exclusive group for --rtsp_url and --camera_number
group = parser.add_mutually_exclusive_group()
group.add_argument(
    "--camera_number",
    help="X from /dev/videoX associated with your video feed.",
    default=0,
    type=int,
)
group.add_argument(
    "--stream_url",
    help="RTSP URL for video feed.",
    type=str,
)

# Parse the command-line arguments
args = parser.parse_args()

if args.stream_url:
    capture_path = args.stream_url
else:
    capture_path = args.camera_number

# Define the template string
image_template = "image-{}.jpg"

dir_name = args.imgdir
if "x" not in args.resolution:
    print("Please specify resolution as WxH. (example: 1920x1080)")
    sys.exit()
img_width = int(args.resolution.split("x")[0])
img_height = int(args.resolution.split("x")[1])

# Create output directory if it doesn't already exist
cwd = os.getcwd()
dir_path = os.path.join(cwd, dir_name)
if not os.path.exists(dir_path):
    os.makedirs(dir_path)

# If images already exist in directory, increment image number so existing images aren't overwritten
# Example: if 'Pics-0.jpg' through 'Pics-10.jpg' already exist, img_num will be incremented to 11.
basename = dir_name
img_num = 1

while True:
    img_name = image_template.format(img_num)
    img_path = os.path.join(dir_path, img_name)
    if os.path.exists(img_path):
        img_num += 1
    else:
        break

# Initialize video
capture = cv2.VideoCapture(capture_path)
capture.set(3, img_width)
capture.set(4, img_height)

# Initialize display window
window_name = 'Press "p" to take a picture!'
cv2.namedWindow(window_name)
cv2.moveWindow(window_name, 50, 30)

print(f"Press p to take a picture. Pictures will automatically be saved in the {dir_name} folder.")
print("Press q to quit.")

while True:
    hasFrame, frame = capture.read()
    cv2.imshow(window_name, frame)

    key = cv2.waitKey(1)
    if key == ord("q"):
        break
    elif key == ord("p"):
        #  Take a picture!
        filename = image_template.format(img_num)
        save_path = os.path.join(dir_path, filename)
        cv2.imwrite(save_path, frame)
        print(f"Picture taken and saved as {filename}.")
        img_num += 1

cv2.destroyAllWindows()
capture.release()
