#! /usr/bin/env bash
CNAME="pycoral"
CAMERA_FEED=$1
IMG_DIR="/app/Pics"
RESOLUTION="1280x720"

docker exec -it -e DISPLAY=:0.0 $CNAME python3 /opt/PictureTaker.py --imgdir "$IMG_DIR" --stream_url "$CAMERA_FEED" --resolution "$RESOLUTION"
