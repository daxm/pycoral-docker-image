#! /usr/bin/env bash
SAMPLE_MODEL_DIR="/opt/sample_model"
CNAME="pycoral"

docker exec -it -e DISPLAY=:0.0 $CNAME python3 /opt/tflite1/TFLite_detection_webcam.py --modeldir=$SAMPLE_MODEL_DIR
