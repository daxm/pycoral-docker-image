#! /usr/bin/env bash
CORAL_DIR="/opt/google-coral/pycoral"
CNAME="pycoral"

docker exec -it $CNAME python3 $CORAL_DIR/examples/classify_image.py --model $CORAL_DIR/test_data/mobilenet_v2_1.0_224_inat_bird_quant_edgetpu.tflite --labels $CORAL_DIR/test_data/inat_bird_labels.txt --input $CORAL_DIR/test_data/parrot.jpg
