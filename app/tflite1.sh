#! /usr/bin/env bash
TFLITE1_DIR="/opt/tflite1"
SAMPLE_MODEL_DIR="/opt/sample_model"
CNAME="pycoral"

docker exec -it -e DISPLAY=:0.0 $CNAME python3 $TFLITE1_DIR/TFLite_detection_webcam.py --modeldir=$SAMPLE_MODEL_DIR
