#! /usr/bin/env bash
SAMPLE_MODEL_DIR="/opt/sample_model"
CNAME="pycoral"
STREAMURL=$1

docker exec -it -e DISPLAY=:0.0 $CNAME python3 /opt/tflite1/TFLite_detection_stream.py --modeldir=$SAMPLE_MODEL_DIR --streamurl="$STREAMURL"
