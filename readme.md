# Google's Coral USB Accelerator

Given the truly ancient requirements to run pycoral in order to use the Google Coral device I've decided to encase its needs into a Docker image.

This image is based on Debian Buster (slim) Docker image and then follows the instructions on setup provided by Google Coral's **Getting Started** page: https://coral.ai/docs/accelerator/get-started/#3-run-a-model-on-the-edge-tpu

## Construction
1. Have docker-compose (or docker compose) installed on your system.
2. Issue `docker-compose build` to build the image from the Dockerfile and docker-compose.yml configuration files' instructions.

## How to Use
- Use `docker-compose up -d` to start the container.  It will remain running **until stopped**.  NoVNC will be available at http://localhost:9090.
- You *should* be able to plug in your Coral USB device into your computer and then use the Docker exec command syntax to run Python scripts against it.  (See Bash scripts in app dir for examples.)
- Write your own python scripts and store them in the app directory.  As that directory is mounted as /app you can execute them using `docker exec` or NoVNC into the container and run them from a terminal.

## Notes
- In order to map the USB device into the Docker container I was lazy and just allowed the container access to **/dev/bus/usb**.  This means that this container does have access to any/all of your USB connected devices.  This may or may not be desirable.
- **/dev/video0:/dev/video0** is commented out in the docker-compose.yml file as you may or may not have a webcam.  It may also not be at /dev/video0.  Uncomment this and set to your webcam devices (eg. **/dev/video3:/dev/video0**).
- It also appears that accessing the USB Accelerator is touchy.  My best advise is to have the device plugged in BEFORE you power on the device (or reboot after you plug it in).  If it still doesn't work (I use the **parrot.sh** script to test), it seems to eventually work.  Just leave your container running and come back **later** and try again.
- You can get to the command line of the container using the **bash_in_container.sh** script.  Then `lsusb` will help you see whether the USB device is present in the container or not.
- OpenCV is installed to obtain camera images and perform object detection.
